const getSum = (str1, str2) => {
  if(typeof str1 !== 'string' && typeof str2 !== 'string') {
  	return false
  } else {
  	if(str1 === '') {
    str1 = 0;
    }
    if(str2 === '') {
    str2 = 0;
    }
    if(isNaN(str1) || isNaN(str2)) {
    	return false;
    }
    return `${+str1 + +str2}`;
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let qwe = []
  let posts = 0;
  let commentors = 0;
  let recursion = (obj, name) => {
    return obj.reduce((a, item) => {
      if (a){
        qwe.push(a);
      }
      if (item.author === name){
        qwe.push(item);
      }
      if (item.comments) {
        return recursion(item.comments, name)
      }
    }, null)
  }
  recursion(listOfPosts, authorName)
  qwe.forEach(item => {
    if(item.comment) {
          commentors++;
    }
    if(item.post) {
      posts++;
    }
  })
  return `Post:${posts},comments:${commentors}`
};

const tickets=(people)=> {
  let cashBox = {
    25:0,
    50:0,
    100:0
  }
  
  for (let i = 0 ; i < people.length ; i++){
    if(people[i] === 25){
      cashBox[25]++;
    }
    
    else if(people[i] === 50){
      if(cashBox[25] !== 0){
        cashBox[25]--;
        cashBox[50]++;
      } else {
        return 'NO';
      }
    }
    
    else if(people[i] === 100){
      if(cashBox[25] !== 0 && cashBox[50] !== 0){
        cashBox[25]--;
        cashBox[50]--;
        cashBox[100]++;
      } else if(cashBox[25] > 2){
        cashBox[25] = cashBox[25] - 3;
        cashBox[100]++;
      } else {
        return 'NO';
      }
    }
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
